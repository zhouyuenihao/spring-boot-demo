package com.xkcoding.zookeeper.config;

import com.xkcoding.zookeeper.annotation.ZooLock;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 首页
 * </p>
 *
 * @author yangkai.shen
 * @date Created in 2018-10-08 14:15
 */
@RestController
public class IndexController {
    @GetMapping(value = "/")
    @ZooLock(key = "test")
    public String index() throws InterruptedException {
        Thread.sleep(5000L);
        return "This is a Spring Boot Admin Client.";
    }
}
